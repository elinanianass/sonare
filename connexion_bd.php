<?php

try {
    $conn = new PDO("mysql:host=localhost;dbname=sonare", 'root','');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connexion à la base de données réussie";
} catch(PDOException $e) {
    echo "Erreur de connexion : " . $e->getMessage();
}

