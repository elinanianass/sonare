<?php
session_start();
include_once 'connexion_bd.php';
try {
        $email = $_POST['email'];
        $stmt = $conn->prepare("SELECT * FROM users WHERE email = :email");
        $stmt->execute(array(':email' => $email));
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user && password_verify($_POST['password'], $user['mot_de_passe'])) {
            $_SESSION['user_id'] = $user['id'];
            header('Location: index.html');
        } else {
            echo "Identifiants incorrects. Veuillez réessayer.";
        }
    
} catch(PDOException $e) {
    echo "Erreur de connexion : " . $e->getMessage();
}
