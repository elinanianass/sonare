<?php
include_once 'connexion_bd.php';

try {
    $password_hashed = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $stmt = $conn->prepare("INSERT INTO users (nom, prenom, email, mot_de_passe) VALUES (?, ?, ?, ?)");
    $stmt->execute([$_POST['nom'], $_POST['prenom'], $_POST['email'], $password_hashed]);

    header('location: login.html');
} catch (PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}



$conn = null;
